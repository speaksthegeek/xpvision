##full screen / Windowed mode

monitor/0/m_is_fullscreen wmgr_mode_fullscreen
monitor/0/m_is_fullscreen wmgr_mode_window

## Number of Monitors and monitor config
num_monitors 3  
monitor/0  
monitor/1  
monitor/2  

##Field of view
monitor/0/proj/FOVx_renopt 44.500000   


##Lat 
monitor/1/proj/off_lat_deg 45.000000
monitor/2/proj/off_lat_deg -45.000000

