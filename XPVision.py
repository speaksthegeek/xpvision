# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'Gui.ui'
#
# Created by: PyQt5 UI code generator 5.8.1
#
# WARNING! All changes made in this file will be lost!



from PyQt5 import QtCore, QtGui, QtWidgets
import sys, os, subprocess

from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QFileDialog, QAction

# TODO number of monitors update NUM

MonitorSetup = """I
10 Version
num_monitors 3
"""

# TODO FOVX update and LATD

Monitor0 = """monitor/0/m_window_idx 0
monitor/0/m_monitor 0
monitor/0/m_window_bounds/0 1924
monitor/0/m_window_bounds/1 314
monitor/0/m_window_bounds/2 3204
monitor/0/m_window_bounds/3 1034
monitor/0/m_arbitrary_bounds/0 0
monitor/0/m_arbitrary_bounds/1 0
monitor/0/m_arbitrary_bounds/2 5760
monitor/0/m_arbitrary_bounds/3 1080
monitor/0/m_bpp 24
monitor/0/m_refresh_rate 0
monitor/0/m_x_res_full 1920
monitor/0/m_y_res_full 1080
monitor/0/m_is_fullscreen wmgr_mode_fullscreen
monitor/0/m_usage wmgr_usage_normal_visuals
monitor/0/proj/diff_FOV 0
monitor/0/proj/FOVx_renopt FOVX00000
monitor/0/proj/FOVy_renopt 25.919115
monitor/0/proj/os_x_rat 0.000000
monitor/0/proj/os_y_rat 0.000000
monitor/0/proj/off_vrt_deg 0.000000
monitor/0/proj/off_lat_deg LATD.000000
monitor/0/proj/off_phi_deg 0.000000
monitor/0/proj/grid_os_on_test 0
monitor/0/proj/grid_os_on_render 0
monitor/0/proj/grid_os_drag_dim_i 10
monitor/0/proj/grid_os_drag_dim_j 10
monitor/0/proj/grid_os_step_dim_i 10
monitor/0/proj/grid_os_step_dim_j 10
monitor/0/proj/grid_os_vert_dim_i 101
monitor/0/proj/grid_os_vert_dim_j 101
monitor/0/proj/grid_os_drag_now_i 32741
monitor/0/proj/grid_os_drag_now_j -10712672
monitor/0/proj/window_2d_off 0
monitor/0/proj/gradient_on 0
monitor/0/proj/gradient_width_top/0 0.000000
monitor/0/proj/gradient_width_top/1 0.000000
monitor/0/proj/gradient_width_ctr/0 0.000000
monitor/0/proj/gradient_width_ctr/1 0.000000
monitor/0/proj/gradient_width_bot/0 0.000000
monitor/0/proj/gradient_width_bot/1 0.000000
monitor/0/proj/gradient_alpha/0/0 1.000000
monitor/0/proj/gradient_alpha/0/1 0.650000
monitor/0/proj/gradient_alpha/0/2 0.330000
monitor/0/proj/gradient_alpha/0/3 0.000000
monitor/0/proj/gradient_alpha/1/0 1.000000
monitor/0/proj/gradient_alpha/1/1 0.650000
monitor/0/proj/gradient_alpha/1/2 0.330000
monitor/0/proj/gradient_alpha/1/3 0.000000"""

# TODO FOVX update and LATD

Monitor1 = """
monitor/1/m_window_idx -1
monitor/1/m_monitor 0
monitor/1/m_window_bounds/0 2556
monitor/1/m_window_bounds/1 29
monitor/1/m_window_bounds/2 3836
monitor/1/m_window_bounds/3 749
monitor/1/m_arbitrary_bounds/0 0
monitor/1/m_arbitrary_bounds/1 0
monitor/1/m_arbitrary_bounds/2 5760
monitor/1/m_arbitrary_bounds/3 1080
monitor/1/m_bpp 24
monitor/1/m_refresh_rate 0
monitor/1/m_x_res_full 1920
monitor/1/m_y_res_full 1080
monitor/1/m_is_fullscreen wmgr_mode_fullscreen
monitor/1/m_usage wmgr_usage_normal_visuals
monitor/1/proj/diff_FOV 0
monitor/1/proj/FOVx_renopt FOVX50000
monitor/1/proj/FOVy_renopt 25.919115
monitor/1/proj/os_x_rat 0.000000
monitor/1/proj/os_y_rat 0.000000
monitor/1/proj/off_vrt_deg 0.000000
monitor/1/proj/off_lat_deg LATD.000000
monitor/1/proj/off_phi_deg 0.000000
monitor/1/proj/grid_os_on_test 0
monitor/1/proj/grid_os_on_render 0
monitor/1/proj/grid_os_drag_dim_i 10
monitor/1/proj/grid_os_drag_dim_j 10
monitor/1/proj/grid_os_step_dim_i 10
monitor/1/proj/grid_os_step_dim_j 10
monitor/1/proj/grid_os_vert_dim_i 101
monitor/1/proj/grid_os_vert_dim_j 101
monitor/1/proj/grid_os_drag_now_i 32741
monitor/1/proj/grid_os_drag_now_j -10712672
monitor/1/proj/window_2d_off 0
monitor/1/proj/gradient_on 0
monitor/1/proj/gradient_width_top/0 0.000000
monitor/1/proj/gradient_width_top/1 0.000000
monitor/1/proj/gradient_width_ctr/0 0.000000
monitor/1/proj/gradient_width_ctr/1 0.000000
monitor/1/proj/gradient_width_bot/0 0.000000
monitor/1/proj/gradient_width_bot/1 0.000000
monitor/1/proj/gradient_alpha/0/0 1.000000
monitor/1/proj/gradient_alpha/0/1 0.650000
monitor/1/proj/gradient_alpha/0/2 0.330000
monitor/1/proj/gradient_alpha/0/3 0.000000
monitor/1/proj/gradient_alpha/1/0 1.000000
monitor/1/proj/gradient_alpha/1/1 0.650000
monitor/1/proj/gradient_alpha/1/2 0.330000
monitor/1/proj/gradient_alpha/1/3 0.000000"""
# TODO FOVX update and LATD

Monitor2 = """
monitor/2/m_window_idx -1
monitor/2/m_monitor 0
monitor/2/m_window_bounds/0 1924
monitor/2/m_window_bounds/1 29
monitor/2/m_window_bounds/2 3204
monitor/2/m_window_bounds/3 749
monitor/2/m_arbitrary_bounds/0 0
monitor/2/m_arbitrary_bounds/1 0
monitor/2/m_arbitrary_bounds/2 5760
monitor/2/m_arbitrary_bounds/3 1080
monitor/2/m_bpp 24
monitor/2/m_refresh_rate 0
monitor/2/m_x_res_full 1920
monitor/2/m_y_res_full 1080
monitor/2/m_is_fullscreen wmgr_mode_fullscreen
monitor/2/m_usage wmgr_usage_normal_visuals
monitor/2/proj/diff_FOV 0
monitor/2/proj/FOVx_renopt FOVX50000
monitor/2/proj/FOVy_renopt 25.919115
monitor/2/proj/os_x_rat 0.000000
monitor/2/proj/os_y_rat 0.000000
monitor/2/proj/off_vrt_deg 0.000000
monitor/2/proj/off_lat_deg LATD.000000
monitor/2/proj/off_phi_deg 0.000000
monitor/2/proj/grid_os_on_test 0
monitor/2/proj/grid_os_on_render 0
monitor/2/proj/grid_os_drag_dim_i 10
monitor/2/proj/grid_os_drag_dim_j 10
monitor/2/proj/grid_os_step_dim_i 10
monitor/2/proj/grid_os_step_dim_j 10
monitor/2/proj/grid_os_vert_dim_i 101
monitor/2/proj/grid_os_vert_dim_j 101
monitor/2/proj/grid_os_drag_now_i 32741
monitor/2/proj/grid_os_drag_now_j -10712672
monitor/2/proj/window_2d_off 0
monitor/2/proj/gradient_on 0
monitor/2/proj/gradient_width_top/0 0.000000
monitor/2/proj/gradient_width_top/1 0.000000
monitor/2/proj/gradient_width_ctr/0 0.000000
monitor/2/proj/gradient_width_ctr/1 0.000000
monitor/2/proj/gradient_width_bot/0 0.000000
monitor/2/proj/gradient_width_bot/1 0.000000
monitor/2/proj/gradient_alpha/0/0 1.000000
monitor/2/proj/gradient_alpha/0/1 0.650000
monitor/2/proj/gradient_alpha/0/2 0.330000
monitor/2/proj/gradient_alpha/0/3 0.000000
monitor/2/proj/gradient_alpha/1/0 1.000000
monitor/2/proj/gradient_alpha/1/1 0.650000
monitor/2/proj/gradient_alpha/1/2 0.330000
monitor/2/proj/gradient_alpha/1/3 0.000000
"""

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        self.fileName = ""

        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(574, 457)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(MainWindow.sizePolicy().hasHeightForWidth())
        MainWindow.setSizePolicy(sizePolicy)
        MainWindow.setMinimumSize(QtCore.QSize(574, 457))
        MainWindow.setMaximumSize(QtCore.QSize(574, 457))
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.pushButton = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton.setGeometry(QtCore.QRect(290, 370, 84, 31))
        self.pushButton.setObjectName("pushButton")
        self.pushButton_2 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_2.setGeometry(QtCore.QRect(380, 370, 84, 31))
        self.pushButton_2.setObjectName("pushButton_2")
        self.Monitor0_spinner = QtWidgets.QSpinBox(self.centralwidget)
        self.Monitor0_spinner.setGeometry(QtCore.QRect(430, 190, 101, 29))
        self.Monitor0_spinner.setMinimum(-180)
        self.Monitor0_spinner.setMaximum(180)
        self.Monitor0_spinner.setProperty("value", 0)
        self.Monitor0_spinner.setObjectName("Montor0_spinner")
        self.Monitor1_spinner = QtWidgets.QSpinBox(self.centralwidget)
        self.Monitor1_spinner.setGeometry(QtCore.QRect(430, 250, 101, 29))
        self.Monitor1_spinner.setMinimum(-180)
        self.Monitor1_spinner.setMaximum(180)
        self.Monitor1_spinner.setProperty("value", 00)
        self.Monitor1_spinner.setObjectName("Monitor1_spinner")
        self.Monitor2_spinner = QtWidgets.QSpinBox(self.centralwidget)
        self.Monitor2_spinner.setGeometry(QtCore.QRect(430, 310, 101, 29))
        self.Monitor2_spinner.setMinimum(-180)
        self.Monitor2_spinner.setMaximum(180)
        self.Monitor2_spinner.setProperty("value", 00)
        self.Monitor2_spinner.setObjectName("Montor2_spinner")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(430, 170, 64, 15))
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(430, 230, 64, 15))
        self.label_2.setObjectName("label_2")
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setGeometry(QtCore.QRect(430, 290, 101, 16))
        self.label_3.setObjectName("label_3")
        self.label_4 = QtWidgets.QLabel(self.centralwidget)
        self.label_4.setGeometry(QtCore.QRect(420, 150, 121, 16))
        self.label_4.setObjectName("label_4")
        self.label_5 = QtWidgets.QLabel(self.centralwidget)
        self.label_5.setGeometry(QtCore.QRect(20, 140, 141, 16))
        self.label_5.setObjectName("label_5")
        self.label_6 = QtWidgets.QLabel(self.centralwidget)
        self.label_6.setGeometry(QtCore.QRect(20, 170, 64, 15))
        self.label_6.setObjectName("label_6")
        self.Max_monitors_spinBox = QtWidgets.QSpinBox(self.centralwidget)
        self.Max_monitors_spinBox.setGeometry(QtCore.QRect(90, 160, 55, 29))
        self.Max_monitors_spinBox.setMinimum(2)
        self.Max_monitors_spinBox.setMaximum(3)
        self.Max_monitors_spinBox.setProperty("value", 3)
        self.Max_monitors_spinBox.setObjectName("Max_monitors_spinBox")
        self.pushButton_3 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_3.setGeometry(QtCore.QRect(10, 70, 111, 31))
        self.pushButton_3.setObjectName("pushButton_3")
        self.plainTextEdit = QtWidgets.QPlainTextEdit(self.centralwidget)
        self.plainTextEdit.setGeometry(QtCore.QRect(10, 30, 531, 31))
        self.plainTextEdit.setObjectName("plainTextEdit")
        self.line = QtWidgets.QFrame(self.centralwidget)
        self.line.setGeometry(QtCore.QRect(20, 110, 521, 20))
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.label_7 = QtWidgets.QLabel(self.centralwidget)
        self.label_7.setGeometry(QtCore.QRect(20, 220, 111, 16))
        self.label_7.setObjectName("label_7")
        self.label_8 = QtWidgets.QLabel(self.centralwidget)
        self.label_8.setGeometry(QtCore.QRect(20, 260, 64, 15))
        self.label_8.setObjectName("label_8")
        self.label_9 = QtWidgets.QLabel(self.centralwidget)
        self.label_9.setGeometry(QtCore.QRect(20, 340, 91, 16))
        self.label_9.setObjectName("label_9")
        self.line_2 = QtWidgets.QFrame(self.centralwidget)
        self.line_2.setGeometry(QtCore.QRect(10, 200, 381, 16))
        self.line_2.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_2.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_2.setObjectName("line_2")
        self.label_10 = QtWidgets.QLabel(self.centralwidget)
        self.label_10.setGeometry(QtCore.QRect(20, 300, 61, 16))
        self.label_10.setObjectName("label_10")
        self.FOV_Spinner_Monitor0 = QtWidgets.QDoubleSpinBox(self.centralwidget)
        self.FOV_Spinner_Monitor0.setGeometry(QtCore.QRect(100, 250, 101, 29))
        self.FOV_Spinner_Monitor0.setDecimals(6)
        self.FOV_Spinner_Monitor0.setMaximum(180.0)
        self.FOV_Spinner_Monitor0.setMinimum(-180.0)
        self.FOV_Spinner_Monitor0.setProperty("value", 0.0)
        self.FOV_Spinner_Monitor0.setObjectName("FOV_Spinner_Monitor0")
        self.FOV_Spinner_Monitor1 = QtWidgets.QDoubleSpinBox(self.centralwidget)
        self.FOV_Spinner_Monitor1.setGeometry(QtCore.QRect(100, 290, 101, 29))
        self.FOV_Spinner_Monitor1.setDecimals(6)
        self.FOV_Spinner_Monitor1.setMinimum(-180.0)
        self.FOV_Spinner_Monitor1.setProperty("value", 00.0)
        self.FOV_Spinner_Monitor1.setObjectName("FOV_Spinner_Monitor1")
        self.FOV_Spinner_Monitor2 = QtWidgets.QDoubleSpinBox(self.centralwidget)
        self.FOV_Spinner_Monitor2.setGeometry(QtCore.QRect(100, 330, 101, 29))
        self.FOV_Spinner_Monitor2.setDecimals(6)
        self.FOV_Spinner_Monitor2.setMinimum(-180.0)
        self.FOV_Spinner_Monitor2.setProperty("value", 00.0)
        self.FOV_Spinner_Monitor2.setObjectName("FOV_Spinner_Monitor2")
        self.pushButton_4 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_4.setGeometry(QtCore.QRect(470, 370, 91, 31))
        self.pushButton_4.setObjectName("pushButton_4")
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 574, 27))
        self.menubar.setObjectName("menubar")
        self.menuFile = QtWidgets.QMenu(self.menubar)
        self.menuFile.setObjectName("menuFile")
        self.menuAbout = QtWidgets.QMenu(self.menubar)
        self.menuAbout.setObjectName("menuAbout")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.actionSave = QtWidgets.QAction(MainWindow)
        self.actionSave.setObjectName("actionSave")
        self.actionQuit = QtWidgets.QAction(MainWindow)
        self.actionQuit.setObjectName("actionQuit")
        self.menuFile.addAction(self.actionSave)
        self.menuFile.addAction(self.actionQuit)
        self.menubar.addAction(self.menuFile.menuAction())
        self.menubar.addAction(self.menuAbout.menuAction())

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "XPvision"))
        self.pushButton.setText(_translate("MainWindow", "Save"))
        self.pushButton_2.setText(_translate("MainWindow", "Exit"))
        self.label.setText(_translate("MainWindow", "Monitor 0"))
        self.label_2.setText(_translate("MainWindow", "Monitor 1"))
        self.label_3.setText(_translate("MainWindow", "Monitor 2"))
        self.label_4.setText(_translate("MainWindow", "lateral Alignment"))
        self.label_5.setText(_translate("MainWindow", "Number of Monitors"))
        self.label_6.setText(_translate("MainWindow", "Max3"))
        self.pushButton_3.setText(_translate("MainWindow", "Set XP11 folder"))
        self.label_7.setText(_translate("MainWindow", "Field of View"))
        self.label_8.setText(_translate("MainWindow", "Monitor 0"))
        self.label_9.setText(_translate("MainWindow", "Monitor 2"))
        self.label_10.setText(_translate("MainWindow", "Monitor 1"))
        self.pushButton_4.setText(_translate("MainWindow", "Launch XP"))
        self.menuFile.setTitle(_translate("MainWindow", "Fi&le"))
        self.menuAbout.setTitle(_translate("MainWindow", "Abo&ut"))
        self.actionSave.setText(_translate("MainWindow", "&Save"))
        self.actionQuit.setText(_translate("MainWindow", "&Quit"))
        # buttons
        self.pushButton_3.clicked.connect(self.browse_file)
        self.pushButton.clicked.connect(self.save_config)
        self.pushButton_4.clicked.connect(self.launch_xplane)
        self.pushButton_2.clicked.connect(self.quit_app)
        # Spinners
        # FOV
        self.FOV_Spinner_Monitor0.valueChanged.connect(self.update_settings_montior0)
        self.FOV_Spinner_Monitor1.valueChanged.connect(self.update_settings_monitor1)
        self.FOV_Spinner_Monitor2.valueChanged.connect(self.update_settings_monitor2)
        # lateral alignment
        self.Monitor0_spinner.valueChanged.connect(self.update_settings_montior0)
        self.Monitor1_spinner.valueChanged.connect(self.update_settings_monitor1)
        self.Monitor2_spinner.valueChanged.connect(self.update_settings_monitor2)
        # Max Number of Monitors
        self.Max_monitors_spinBox.valueChanged.connect(self.update_settings_max_monitors)
        # File Menu
        self.menuAbout.triggered.connect(self.update_settings)
        self.actionSave.triggered.connect(self.save_config)
        self.actionQuit.triggered.connect(self.quit_app)
        self.menuAbout.triggered.connect(self.about)

        # status bar
        self.statusbar.showMessage('Ready')

    def about(self):
        pass

    def browse_file(self):
        self.fileName, _ = QFileDialog.getOpenFileName(filter="X-Plane-x86_64", caption="Select X-Plane11 Executable")
        if self.fileName != "":
            self.plainTextEdit.setPlainText(self.fileName)
            self.filePath, self.file = os.path.split(self.fileName)
        else:
            self.plainTextEdit.setPlainText("invalid selection")


    def update_settings(self):
        print("something updated")
        self.statusbar.showMessage("Updated something")
        self.update_savefile()

    def update_settings_montior0(self):
        print("Updated monitor0 settings")
        self.statusbar.showMessage("Updated monitor0 settings")
        self.update_savefile()

    def update_settings_monitor1(self):
        self.statusbar.showMessage("Updated monitor1 settings")
        self.update_savefile()


    # TODO FOVX update and LATD
    def update_settings_monitor2(self):
        print("Updated monitor2 settings")
        self.statusbar.showMessage("Updated monitor2 settings")
        self.update_savefile()

    # TODO FOVX update and LATD
    def update_settings_max_monitors(self):
        print("something monitors")
        self.statusbar.showMessage("Updated Monitors")
        self.update_savefile()


    def save_config(self):
        if self.fileName == "":
            self.plainTextEdit.setPlainText("Error XPlane11 folder not set")
        else:
            print("save config to " + self.filePath)

    def launch_xplane(self):
        if self.fileName == "":
            self.plainTextEdit.setPlainText("Error XPlane11 folder not set")
        else:
            print("save config to " + self.filePath)
            XP11 = subprocess.Popen(self.fileName)

    def update_savefile(self):
        MonitorConfig = MonitorSetup.replace("NUM", str(self.Max_monitors_spinBox.value()))

        config1 = Monitor0.replace("FOVX", str(self.FOV_Spinner_Monitor0.value()))
        MasterConfig0 = config1.replace("LATD", str(self.Monitor0_spinner.value()))
        config2 = Monitor1.replace("FOVX", str(self.FOV_Spinner_Monitor1.value()))
        MasterConfig1 = config2.replace("LATD", str(self.Monitor1_spinner.value()))
        config3 = Monitor2.replace("FOVX", str(self.FOV_Spinner_Monitor2.value()))
        MasterConfig2 = config3.replace("LATD", str(self.Monitor2_spinner.value()))
        save_data = MonitorConfig + MasterConfig0 + MasterConfig1 + MasterConfig2
        try:
            with open(self.fileName+"Output/preferences/X-Plane Window Positions.prf", "w") as myXPconfig:
                for info in save_data:
                    myXPconfig.write(info)
        except:
            print("error saving data")


    def quit_app(self):
        sys.exit(0)


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    ex = Ui_MainWindow()
    app.setWindowIcon(QtGui.QIcon('resources/x-plane.png'))
    w = QtWidgets.QMainWindow()
    ex.setupUi(w)
    w.show()
    sys.exit(app.exec_())
